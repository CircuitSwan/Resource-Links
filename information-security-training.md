Here are resources for information security training

- https://github.com/bugcrowd/bugcrowd_university

# learning about code
- Compiler Explorer http://godbolt.org identifies an assembly based on a code snipit

# Capture the Flags or intentionally compromised sites
- http://192.241.201.75:8881/
- https://www.hackthebox.eu/
- https://community.securityinnovation.com/

# To Sort
- https://twitter.com/WebSecAcademy/status/1229439864556290048
- https://portswigger.net/web-security/cross-site-scripting/exploiting/lab-stealing-cookies?utm_source=twitter&utm_medium=social&utm_campaign=existing-labs 
- vdalabs.com
- CodeBashing
- Hunter2
- AppSec Labs
- OWASP RailsGoat
- OWASP SecurityNinjas
- Manicode Security
- https://www.owasp.org/index.php/OWASP_Juice_Shop_Project
- Vulnhub.com
- OverTheWire.org
- Hackthebox.eu
- https://trailofbits.github.io/ctf/
- https://kitctf.de/learning/
- https://exploit-exercises.com/
- http://pwnable.kr/
- https://google-gruyere.appspot.com/
- http://overthewire.org/wargames/
- https://www.hacking-lab.com/index.html
- http://www.potatopla.net/crypto/
- http://www.securitytube.net/
- http://www.hackthissite.org/pages/index/index.php
- http://www.porcupine.org/satan/admin-guide-to-cracking.html
- https://twitter.com/RealTryHackMe
- http://pwnable.xyz
- https://twitter.com/CircuitSwan/status/1163551924005871617
- https://github.com/NicoleSchwartz/Learning-to-code-resources/wiki/Secure-Coding-or-Information-Security 
