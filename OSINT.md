# OSINT resources

- https://stormctf.ninja/ctf/blog/stormctf/bellebytes-osint-guide
- [#OSINT] For beginners using OSINT, learning advanced search operators may be the best way to start. Google Dorks, Google's advanced search operators allow you to find content within a very narrow scope which can be unprotected and possibly sensitive. https://jakecreps.com/2018/09/10/osint-applications-for-google-dorks/