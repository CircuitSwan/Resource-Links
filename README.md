# Resource-Links

A collection of resource links I use or want to use

## So you want to learn more about the security space

security keeps specializing and there are sooooo many parts of infosec now - you can be into just one thing, or all the things

### read these other great guides

- https://danielmiessler.com/blog/build-successful-infosec-career/
- https://gist.github.com/mubix/5737a066c8845d25721ec4bf3139fd31
- https://www.sans.org/blog/getting-started-in-cybersecurity-with-a-non-technical-background/
- https://www.eanmeyer.com/p/start.html
- https://dfirmadness.com/getting-into-infosec/the-five-pillars/
- https://www.trustedsec.com/blog/breaking-into-infosec-a-beginners-guide-part-1/

hack4pancakes
- https://tisiphone.net/2015/10/12/starting-an-infosec-career-the-megamix-chapters-1-3/
- https://tisiphone.net/2015/11/08/starting-an-infosec-career-the-megamix-chapters-4-5/
- https://tisiphone.net/2016/02/10/starting-an-infosec-career-the-megamix-chapter-6/

### books

- check out no starch press

### Watch conference videos on youtube:
 - Pancakescon https://www.youtube.com/channel/UCAnBQGSBDlEbq11dkAreUGg
 - @OURSAConference https://youtu.be/pcUDzHb4Gdo
 - Diana Initiative https://www.youtube.com/thedianainitiative
 - Day of Shecurity https://www.youtube.com/channel/UCYg9C2U7l_IMO4Qo4L3HM2w
 - DEF CON / DEFCON https://www.youtube.com/user/DEFCONConference & https://www.youtube.com/channel/UCWlRwmWMQic67ntTn38u94g
 - HOPE (hackers on planet earth) https://www.youtube.com/channel/UC_GRMAkr6nIEx8smcOH1CNw
 - BSidesLV https://www.youtube.com/channel/UCpNGmljppAJbTIA5Msms1Pw
 - Shmoocon https://archive.org/details/ShmooCon_2020
 - LocoMocoSec https://www.youtube.com/channel/UCGjspAdr5_bqdC-OtGuJkvg
 - DerbyCon

 And so many more - Information Security is huge this can help you figure out which topics are interesting to you!

#### Specific talks I recommend

LocoMocoSec: Hawaii Product Security Conference
 - https://www.youtube.com/watch?v=HDo2iOlkbyc
 - https://www.youtube.com/watch?v=73nJvOhMWfA
 - https://www.youtube.com/watch?v=lpOEFube034
 - https://www.youtube.com/watch?v=HtsXfEbIMco
 - https://www.youtube.com/watch?v=89l2ks9DPUE
 - https://www.youtube.com/watch?v=FIWyk--nvKE
 - https://www.youtube.com/watch?v=M3eq6-2kv-k&t=163s

### Find a local group

For example - a local DEF CON group, a local information security group - search for them, and check meetup and 2600

### Attend a conference!

BSides Security conferences are small local conferences for security people
http://www.securitybsides.com/w/page/12194156/FrontPage

Specific BSides as examples
 - http://www.bsidesdc.org/
 - http://2019.bsidescharm.com/
 - http://www.bsidesnova.org/

There are so many events - look around!

list here: https://twitter.com/i/lists/219115889 (I am happy to add more)

#### Hacker Summer Camp (BlackHat, BSidesLV, DEF CON and Diana Initiative)

I wrote this guide specifically for Hacker Summer Camp (BlackHat, BSidesLV, DEF CON and Diana Initiative) http://hackersummercamp.guide 

Also bsides Las Vegas happens right before defcon it's virtual this year and in the past they have had student scholarships so I would check their website
And as a student you can absolutely attend the Diana Initiative which is in July the usually occurs at the same time as defcon for free and you may be able to meet up with people or going to defcon so that you know some people

Diana initiative is also virtual this year

You're at a good advantage this year because defcon is hybrid so you don't have to pay for a ticket in order to attend so that's at least three hacking conferences you can attend a live for free assuming besides Las Vegas still does students scholarships

#### Cost?

There are scholarships - look around at each event - for example women in cyber security (wicys) group https://twitter.com/WiCySorg?s=09 often do scholarships
https://twitter.com/WiCySorg for DEF CON

Volunteer! Many events give free badges for volunteering (still leaves you with hotel) which works well locally, there is likely a local event. Some events do often cover room and food! ask. Bonus, you'll be forced out of your shell (if thats a problem for you) and have to meet people.

once you volunteer (we have chronic volunteers we lovingly call infosec roadies) you'll get to know other chronic volunteers and eventually the organizers, say hi, but usually they are running around crazy busy during the event - way in advance, or a few weeks after is alwas a better time for just chit chat

If you are looking at DEF CON specifically - check out all the villages and events - https://twitter.com/i/lists/1147466848465752065
You can also use that list it should have most of the villages on there and you're unfortunately going to have to check each village to see if they're currently actively recruiting volunteers. you'll also have to ask if it gets you a badge because each village only gets a certain number of badges so not all villages will give you a free badge for volunteering

#### Making friends / networking

The hackers worth knowing will talk to you - doesn't matter if they are "famous" if they blow you off they aren't worth it. so go in with curiousity - ask questions, try things, fail, try again, and enjoy

discord, slack and twitter are all pretty popular - once you find a specific interest(s) like Ham radio, lockpicking, OSINT, ctf etc you'll end up with invites to some - which is great for making friends in advance.

### Hands-On

Some things (like SANS) have scholarships occasionally, keep an eye out! and sometimes conferences or companies have super cheap / discounted / scholarship for classes. when it doubt it can't hurt to contact them and ask.

#### Home lab

read some blogs, try some things hands on

specific suggested book/lab - "Building Virtual Labs: A hands-on guide"

#### CTF

Capture the flag! there are many free online or at events. give it a go. And if you don't like it thats OK you don't have to.

you can usually find a crew to do it with and many writeups (for perhaps expired challenges) to read and see techniques

#### Certs / classes

Look for scholarships or discounts or freebies - often cloud providers have free events and give out free credits! There is a huge debate about how useful certs are, they may help you get a job, but you can learn everything for a cert for free if you hunt around (or cheap, buy a book)

### learning resources
 - https://github.com/NicoleSchwartz/Learning-to-code-resources/wiki < go to "learning to code securely..." https://github.com/NicoleSchwartz/Learning-to-code-resources/wiki/Secure-Coding-or-Information-Security then there is a link to hack the box
 - https://about.gitlab.com/handbook/marketing/product-marketing/competitive/application-security/

### Mentors

check out #MentoringMonday and #cybermentoringmonday - make your own post looking for one!

try https://securitymentor.club

## Check out more about Secure at GitLab specifically

 - https://about.gitlab.com/handbook/marketing/product-marketing/demo/ demos
 - https://about.gitlab.com/solutions/dev-sec-ops/ DevSecOps
 
 ## GitLab elevator pitch
  - https://about.gitlab.com/handbook/marketing/product-marketing/messaging/#gitlab-value-proposition
  - “GitLab is a single application which allows engineers to collaborate more efficiently, iterate faster and more securely, and produces results more efficiently.” Thomas Woodham
