# Networking resources

- "How to use the  #Shodan command-line interface to setup real-time network monitoring for your home IP address." https://asciinema.org/a/231048

## Burp
- [Download Community Edition Burp suite](https://portswigger.net/burp/communitydownload)
- [Notes on setup](burp-suite.md)