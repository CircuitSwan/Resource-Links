# Security Companies to look at and note that they do

- (aka vendors reaching out to me, look at, make notes, decide if worth follow up)
- [cohesity.com](cohesity.com)
- [mongodb.com](mongodb.com)
- [konghq.com](konghq.com)
- [cockroachlabs.com](cockroachlabs.com)
- [signalfx.com](signalfx.com) "As a quick refresher, SignalFx is a cloud infrastructure and application monitoring tool built to provide the speed and scale needed for today's cloud-native stacks. We just recently were acquired by Splunk, completing the observability trifecta to cover metrics + logs + traces in one solution."
- [fugueinc.com](fugueinc.com) 
- [cloudzero.com](cloudzero.com) "CloudZero is new kind of cost management platform designed to meet the needs of engineering teams at cloud companies.  We help you detect spend, give you engineering context to understand what happened, and help you measure your cloud COGS by the metrics that matter to your team (for example, the cost to run a product feature). We offer incredibly fast, granular cost data to help you understand how and why your bill is changing."
- [pagerduty.com](pagerduty.com) 
