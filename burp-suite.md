Based on bugcrowd class

- https://github.com/bugcrowd/bugcrowd_university
- Slides https://github.com/bugcrowd/bugcrowd_university/blob/master/An_introduction_to_Burp_Suite/Bugcrowd%20University%20-%20Burp%20Suite%20Introduction.pdf
- Video https://www.bugcrowd.com/resource/introduction-to-burp-suite/

# Burp Setup
- Optional-Set firefox profile to a new profile ("Burp")
  - Windows - Run "firefox.exe -P" then "create a new profile" or pick one, start firefox
  - Mac
  - Linux
![firefox user profile](images/firefox_profile.png "firefox user profile")
- Open firefox and set your proxy settings "network" about:preferences#searchResults
![firefox proxy settings](images/burp_firefox_proxy.png "firefox proxy settings")
- http://burp/ and save CA file
![firefox download CA](images/firefox_burp_ca.png "firefox download CA")
- view "certificates" in firefox and import on authories tab the one we just downloaded about:preferences#searchResults
![firefox import ca](images/burp_firefox_ca.png "firefox import ca")
- check the "trust websites" checkbox
![firefox save ca](images/burp_firefox_save_ca.png "firefox save ca")
- Turn off intercept in burp
![burp intercept setting](images/burp_intercept.png "burp intercept setting")
- View a website in firefox
- view the "http intercept" in burp
![burp http history](images/burp_http_history.png "burp http history")
- Setup your scope
  - Use advanced scope control, click add
  - focus on the specific website or item you want to see

# Suggested Firefox plugins for playing with burpsuite
- https://addons.mozilla.org/en-US/firefox/addon/wappalyzer/
- https://addons.mozilla.org/en-US/firefox/addon/builtwith/
- https://addons.mozilla.org/en-US/firefox/addon/hackbartool/
- https://addons.mozilla.org/en-US/firefox/addon/uaswitcher/
- https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/
- https://addons.mozilla.org/en-US/firefox/addon/web-developer/ ?