# Pro Remote Work

- Forbes - [Why Companies Should Let Employees Work Remotely And Travel More](https://www.forbes.com/sites/meimeifox/2018/05/17/why-companies-should-let-employees-work-remotely-and-travel-more/#1420ac694bca)
- [Book: Remote: Office not required](https://smile.amazon.com/Remote-Office-Required-Jason-Fried/dp/0804137501/ref=sr_1_1?s=books&ie=UTF8&qid=1539567654&sr=1-1&keywords=remote)
- https://medium.com/@chandrasekaran.satish/distributed-teams-are-here-to-stay-strategies-to-help-them-succeed-5705a3408c2c 
- https://medium.com/@satishchandrasekaran/strategies-for-a-new-distributed-team-to-succeed-bd77f0541708 

> People are sick of the rat-race, eager to take control of their lives, and desperate to find a balance between work and life.
>
> – Two-thirds of people want to work from home.
>
> – 36% would choose it over a pay raise.
>
> – A poll of 1,500 technology professionals revealed that 37% would take a pay cut of 10% if they could work from home.
>
> – Gen Y’ers are more difficult to recruit (as reported by 56% of hiring managers) and to retain (as reported by 64% of hiring managers), but they are particularly attracted to flexible work arrangements (rating among benefits as an 8 on a 10 scale for impact on overall job satisfaction).
>
> – 80% of employees consider telework a job perk.
>
> http://globalworkplaceanalytics.com/resources/costs-benefits 

# Pro Flexible hours

> Flextime provides an opportunity for employees to match their work schedules to their own circadian rhythms. However, managers often destroy this opportunity to capture value by punishing employees for using schedules that match an owl’s rhythm. In my own research, I found that supervisors tend to assume that employees who start and finish work late (versus early) are less conscientious and lower in performance, even if their behavior and performance is exactly the same as someone working an early riser’s schedule. Managers must see past their own biases if they want to optimize schedules in order to match the most important activities to the natural energy cycles of employees. Managers who do this will have energized, thriving employees rather than sleepy, droopy employees struggling to stay awake. Your most important tasks deserve employees who are working when they’re at their best.
>
> https://hbr.org/2015/01/the-ideal-work-schedule-as-determined-by-circadian-rhythms 

# Anti-open-office plan

- [How the modern office is killing our creativity](https://www.ft.com/content/6148ec14-457a-11e9-a965-23d669740bfb?accessToken=zwAAAWmBMsFIkc9hSOwURXoR6dOpZSPWaXQL-w.MEUCIQDeEI81iN-ZhiIiDUuRlHkA2cyNVS5gTCDJcUc5l0k28gIgH4vsJ_xFEqBrRec5zUHfphDgGsjJVAQta94VIO4O0vc&sharetype=gift?token=8479f768-8067-4277-adfd-2cb6cfff277b)
- [On the effect of open offices: "the volume of face-to-face interactions decreased significantly (approx. 70%)"(http://rstb.royalsocietypublishing.org/content/royptb/373/1753/20170239.full.pdf)

> One of the leading complaints about open offices is the noise, and research shows that the constant pandemonium can actually sabotage motivation. A study from the Journal of Applied Psychology subjected 40 workers to three hours of “low-intensity noise” designed to simulate the sounds of an open office. Meanwhile, a control group experienced three hours of quiet. After the three hour period had ended, the groups were given puzzles to solve — puzzles that had no solution. The workers who’d been treated to a quiet setting continued to hammer away at the puzzles, while the subjects who’d experienced noisy work conditions gave up after fewer attempts.
> 
> https://www.psypost.org/2016/08/how-open-offices-are-killing-us-44478 

> But research that we’re 15% less productive, we have immense trouble concentrating and we’re twice as likely to get sick in open working spaces, has contributed to a growing backlash against open offices.
> 
> http://www.bbc.com/capital/story/20170105-open-offices-are-damaging-our-memories 

> One study of more than 1,800 Swedish workers found that people in open plan offices were nearly twice as likely to take short term sick leave (of one week or less) than those who worked in private offices. A survey from Denmark showed employees in open plan offices were 62 percent more likely to take a sick day than those with their own separate office. Another survey from Canada had similar results, with open office workers taking an average of 3.1 sick days in a year, compares to 1.8 sick days for employees who worked from home.
>
> https://motherboard.vice.com/en_us/article/z43nby/is-your-open-office-making-you-sick 

> In previous posts, I've provided links to numerous studies showing that open-plan offices are both a productivity disaster and a false economy. (The productivity drain more than offsets the savings in square footage.) … "All of this social engineering has created endless distractions that draw employees' eyes away from their own screens. Visual noise, the activity or movement around the edges of an employee's field of vision, can erode concentration and disrupt analytical thinking or creativity."
>
> https://www.inc.com/geoffrey-james/science-just-proved-that-open-plan-offices-destroy-productivity.html 

> A key takeaway from our study is that the open plan isn’t to blame any more than reverting to all private offices can be a solution. There is no single type of optimal work setting. Instead, it’s about balance. Achieving the right balance between working in privacy and working together is critical for any organization that wants to achieve innovation and advance.
> Office workers are interrupted as often as every three minutes by digital and human distractions. These breaches in attention carry a destructive ripple effect because, once a distraction occurs, it can take as much as 23 minutes for the mind to return to the task at hand, according to recent research done at the University of California.
>
> https://www.steelcase.com/research/articles/privacy-crisis/

> According to head researcher Dr. Vinesh Oommen, “The outcome of working in an open-plan office was seen as negative, without offices causing high levels of stress, conflict, high blood pressure and a high staff turnover.””
>
> http://www.cfodailynews.com/are-open-plan-offices-bad-for-work/

> In June, 1997, a large oil and gas company in western Canada asked a group of psychologists at the University of Calgary to monitor workers as they transitioned from a traditional office arrangement to an open one. The psychologists assessed the employees’ satisfaction with their surroundings, as well as their stress level, job performance, and interpersonal relationships before the transition, four weeks after the transition, and, finally, six months afterward. The employees suffered according to every measure: the new space was disruptive, stressful, and cumbersome, and, instead of feeling closer, coworkers felt distant, dissatisfied, and resentful. Productivity fell.
>
> https://www.newyorker.com/business/currency/the-open-office-trap 

# Sample letter I have sent to employers

*Pick your term, telework, remote work, distributed work, and define it if it is not already defined internally.*

It has been my plan to push for more telework work and flexible work hours.

I have tried to articulate the advantages of telework work below, having previously worked in environments where we started co-located, changed to multi-office, and eventually became almost completely telework.

## The positive impact of flexible telework

### Benefits of telework policies for employers

*Recruitment* – Telework is considered a valuable perk for candidates, and has the potential to draw a larger pool of applicants. Previously, candidates have withdrawn their applications due to our current telework policies. 

*Retention* – When associate’s personal circumstances change and prevent them from continuing to work full time within the office, telework could improve associate retention. We have frequently lost associates to competing offers because the competition permitted telework. This would have the added benefit of a reduction in the cost of training new team members to use our tools and processes. We have had 100% telework associates before, and the path to it could be standardized. 

*Growth* - The company has issues providing adequate space for associates, which we anticipate will create further difficulties as the business grows. Increasing telework will reduce the need for physical space, reducing company overhead costs associated with acquiring more space, parking, and redesigns. 

Due to meeting rooms being used as war rooms and quiet work areas, we have teams who are working within close physical proximity (the same office) frequently using WebEx for meetings. Teleworking would allow the company to utilize the meeting rooms for meetings. “Hot desks” or shared desks with increased telework would also contribute to resolving this issue.

### Benefits of telework policies for associates

*Family* – telework would allow associates to work during the day while still Allowing them to meet commitments which occur directly after work but far from the office. This would allow for a greater sense of connectedness to critical support networks. 

*Resolution of Open Office Issues* - Noise levels within open offices create a challenging environment in which to concentrate on work. Currently, associates use a meeting room when they require a quiet space.  Unfortunately, the number of associates who wish to use these rooms is larger than the number of available meeting rooms.

*Commute* - telework reduces commute time, and permits work location to be flexible based on associates needs and work project needs. 

## What makes for good teamwork when teams are not co-located?

Effective telework requires communication, written policies and procedures, and conscientious action on the part of the teams; therefore it is important that these policies are well planned.

A majority of our team is currently not co-located (India, Sri Lanka), and anyone can be working from home (for their one day telework), so our teams are already conscientious about teamwork across timezone and geographical differences. 

### Collaboration

Due to our current policy (one telework day with varied schedules, and half the team offshore), all meetings require video conferencing. We communicate and collaborate effectively using our current tools; WebEx, Jira, Confluence, and Microsoft Communicator. We also use these tools to collaborate with our customers within the IT department, and our sources and customers outside of the IT department.

### Deadlines

When teleworking associates are managed effectively, deadlines continue to be met. Associates will continue to be evaluated on their deliverables. We would continue to use teletrack, Jira, and weekly 1:1s to evaluate this.

### Communication with telework associates

Telework associates will remain accessible to their team leaders and team members for daily scheduled core hours. A further benefit of telework is the reduction in, or proper timing of, distractions from colleagues such as “just one quick question.”


## First Steps

### Rolling out these changes:

Step one: Permit associates who have been with the company for multiple years to telework more than one day a week.

Evaluation of step one: Velocity of the team and the individual will be monitored to ensure it continues at the same pace as before the increase in telework. This will occur over a 6 month period. Quality of the team’s work (re-work and bugs) is expected to stay steady, or decrease over a six month period.
Step two: Set a minimum standard (for example 1 year employment, and a specific minimum performance rating) which allows associates who are performing well a full telework option. Associates should also have successfully maintained one day of telework per week for at least 6 months without changes to their quantity and quality of work.

### Policies

We currently have telework policies in place that can be enhanced. For example: 
- Available Time: Associates are expected to be available for core hours, and additional hours to equal weekly hours as indicated in their availability. They would communicate their availability in Outlook. 
- No competing needs: such as child care or elder care
- Explicitly stated performance rating that must be maintained
- Seniority: minimum for 1 day is 6 months, minimum for >1 day is 12 months and prior successful telework
- Minimum internet speed
- Access to a phone